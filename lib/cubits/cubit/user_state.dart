part of 'user_cubit.dart';

@immutable
abstract class UserState {
  List<UserModel>  get users => [];
}

class UserInitial extends UserState {}

class UserLoading extends UserState {}

class UserLoadError extends UserState {
  final String error;
  UserLoadError(this.error);
}

class UserFinishState extends UserState {
  final List<UserModel> _users;
  UserFinishState(this._users);

  @override
  List<UserModel> get users => _users;
}
