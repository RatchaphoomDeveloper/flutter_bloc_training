import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blocs/controllers/user_api.controller.dart';
import 'package:flutter_blocs/models/users/users.mode.dart';
import 'package:flutter_blocs/reponsitory/user/users.responsitory.dart';
import 'package:meta/meta.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());
  Future<void> fetchData() async{
    try {
      emit(UserLoading());
      debugPrint("Loading.....");
      final _data = await UsersResponsitory.getUsers();
      emit(UserFinishState(_data));
    } catch (e) {
      emit(UserLoadError("Feching Data is error"));
    }
  }
}
