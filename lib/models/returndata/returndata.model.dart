class ReturnDataModel {
  dynamic? errors;
  int? code;
  String? message;
  dynamic? data;

  ReturnDataModel({
    this.code,
    this.message,
    this.data,
    this.errors
  });

  ReturnDataModel.fromJson(Map<String,dynamic>json){
    code = json['code'];
    message = json['message'];
    data = json['data'];
    errors = json['errors'];
  }

  Map<String,dynamic> toJson(){
    final Map<String,dynamic> json = <String,dynamic>{};
    json['code'] = code;
    json['message'] = message;
    json['data'] = data;
    json['errors'] = errors;
    return json;
  }
}