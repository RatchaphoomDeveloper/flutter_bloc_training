class UserModel {
  int? id;
  String? username;
  String? email;
  String? phone;
  String? website;

  UserModel({this.id, this.username, this.email, this.phone, this.website});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    website = json['website'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = <String, dynamic>{};
    json['id'] = id;
    json['username'] = username;
    json['email'] = email;
    json['phone'] = phone;
    json['website'] = website;
    return json;
  }
}
