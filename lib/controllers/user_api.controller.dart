import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blocs/configs/api_path.config.dart';
import 'package:flutter_blocs/dio/dio.helper.dart';
import 'package:flutter_blocs/models/returndata/returndata.model.dart';

import '../configs/env.config.dart';

class UserApiController {
  static final String _dataUrl = "${EnvConfigOptions.apiUrl}";

  static Future<ReturnDataModel> getUsers() async {
    try {
      Response response = await DioHelper.baseAPI
          .get("${_dataUrl}${ApiPathOptions.getUsersPath}");
      Map<String,dynamic> data = ReturnDataModel(code : 1,message: 'Success',data: response.data).toJson();
      return ReturnDataModel.fromJson(data);
    } on DioError catch (e) {
      if (e.response!.data is String) {
        return ReturnDataModel(code: -1, message: 'Error', data: null);
      }
      return ReturnDataModel.fromJson(e.response!.data);
    }
  }
}
