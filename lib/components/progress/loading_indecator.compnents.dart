import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class LoadingIndecatorCompnents extends StatelessWidget {
 final double scale;
 const LoadingIndecatorCompnents({ Key? key,  this.scale = 1 }) : super(key: key);
 Widget _getIndicator(){
  switch(defaultTargetPlatform){
    case TargetPlatform.iOS:
      return const CupertinoActivityIndicator(
        radius: 20,
      );
    default:
      return const CircularProgressIndicator();

  }
 }
  @override
  Widget build(BuildContext context){
    return Container(
      color: Colors.white,
      child: Center(
        child: Transform.scale(
          scale: scale,
          child: _getIndicator(),
        ),
    ),
    );
  }
}