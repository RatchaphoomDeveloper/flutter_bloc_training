import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../models/users/users.mode.dart';
import '../../reponsitory/user/users.responsitory.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserLoading()) {
    on<LoadUserEvent>(_onLoadUserEvent);
    on<AddUserEvent>(_onAddUserEvent);
    on<UpdateUserEvent>(_onUpdateUserEvent);
    on<DeleteUserEvent>(_onDeleteUserEvent);
  }

  void _onLoadUserEvent(LoadUserEvent event, Emitter<UserState> emit) async {
    final _data = await UsersResponsitory.getUsers();
    emit(UserFinishState(_data));
  }

  void _onAddUserEvent(AddUserEvent event, Emitter<UserState> emit) {
    final state = this.state;
    if(state is UserFinishState){
      emit(UserFinishState(List.from(state.users)..add(event.userModel)));
    }
  }
  void _onUpdateUserEvent(UpdateUserEvent event, Emitter<UserState> emit) {}
  void _onDeleteUserEvent(DeleteUserEvent event, Emitter<UserState> emit) {}
}
