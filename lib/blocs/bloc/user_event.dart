part of 'user_bloc.dart';

@immutable
abstract class UserEvent {
  List<UserModel> get users => [];
}

class LoadUserEvent extends UserEvent {
  final List<UserModel> userModel;
  LoadUserEvent({this.userModel = const <UserModel>[]});

  @override
  List<UserModel> get users => userModel;
}

class AddUserEvent extends UserEvent {
  final UserModel userModel;
  AddUserEvent({required this.userModel});

  @override
  List<UserModel> get users => [userModel];
}

class DeleteUserEvent extends UserEvent {
  final UserModel userModel;
  DeleteUserEvent({required this.userModel});

  @override
  List<UserModel> get users => [userModel];
}

class UpdateUserEvent extends UserEvent {
  final UserModel userModel;
  UpdateUserEvent({ required this.userModel});
  @override
  List<UserModel> get users => [userModel];
}
