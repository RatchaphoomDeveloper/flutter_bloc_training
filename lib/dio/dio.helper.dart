import 'package:dio/dio.dart';
import 'package:flutter_blocs/configs/env.config.dart';

class DioHelper {
  static BaseOptions opts = BaseOptions(
    // baseUrl: EnvConfigOptions.apiUrl,
    connectTimeout: const Duration(seconds: 30000),
    receiveTimeout: const Duration(seconds: 30000),
  );

  static Dio createDio() {
    return Dio(opts);
  }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(InterceptorsWrapper(
        onResponse: (Response response, ResponseInterceptorHandler handler) =>
            responseInterceptor(response, handler),
        onError: (DioError e, ErrorInterceptorHandler handler) {
          return handler.next(e);
        },
      ));
  }

  static dynamic responseInterceptor(
      Response response, ResponseInterceptorHandler handel) {
    handel.next(response);
  }

  static final dio = createDio();
  static final baseAPI = addInterceptors(dio);
}
