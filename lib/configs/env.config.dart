import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvConfigOptions {
  static String get fileName => kReleaseMode ? ".env.production" : ".env";
  static String get apiUrl => dotenv.env["WEB_SERVICE"] ?? "Can't load WEB_SERVICE";
}