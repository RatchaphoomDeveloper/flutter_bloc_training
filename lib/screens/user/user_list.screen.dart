import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs/blocs/bloc/user_bloc.dart';
import 'package:flutter_blocs/screens/user/user_form.screen.dart';

import '../../components/progress/loading_indecator.compnents.dart';

class UserListScreen extends StatefulWidget {
  const UserListScreen({Key? key}) : super(key: key);

  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is UserLoading || state is UserInitial) {
          return const LoadingIndecatorCompnents(
            scale: 1,
          );
        }
        if (state is UserFinishState) {
          return Scaffold(
              appBar: AppBar(
                // Here we take the value from the MyHomePage object that was created by
                // the App.build method, and use it to set our appbar title.
                title: Text("User List"),
                actions: [IconButton(onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=>UserFormScreen())
                  );
                }, icon: Icon(Icons.add))],
              ),
              body: Padding(
                  padding: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    child: Column(children: [
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: state.users.length,
                          itemBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(
                                    top: 20, bottom: 20, left: 10),
                                margin: EdgeInsets.only(bottom: 20),
                                width: double.infinity,
                                decoration: BoxDecoration(color: Colors.amber),
                                child: Row(children: [
                                  Expanded(
                                      flex: 1,
                                      child: Column(
                                        children: [
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                "${state.users[index].username}"),
                                          )
                                        ],
                                      )),
                                ]),
                              ),
                            );
                          })
                    ]),
                  )));
        } else {
          return Scaffold(
            body: Center(child: Text("${state}")),
          );
        }
      },
    );
  }
}
