import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs/blocs/bloc/user_bloc.dart';
import 'package:flutter_blocs/components/progress/loading_indecator.compnents.dart';
import 'package:flutter_blocs/configs/env.config.dart';
import 'package:flutter_blocs/controllers/user_api.controller.dart';
import 'package:flutter_blocs/cubits/cubit/user_cubit.dart';
import 'package:flutter_blocs/screens/user/user_list.screen.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:loader_overlay/loader_overlay.dart';

Future<void> main() async {
  await dotenv.load(fileName: EnvConfigOptions.fileName);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(const MyApp()));
  // await UserApiController.getUsers();
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    // return
    return GlobalLoaderOverlay(
      useDefaultLoading: false,
      overlayWidget: const LoadingIndecatorCompnents(),
      child: MultiBlocProvider(
        providers: [
          // BlocProvider(create: (_) => UserCubit()..fetchData()),
          BlocProvider(create: (_)=>UserBloc()..add(
            LoadUserEvent()
          ))
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: UserListScreen(),
        ),
      ),
    );
  }
}


