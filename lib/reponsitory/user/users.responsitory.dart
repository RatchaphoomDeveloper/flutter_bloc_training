import 'package:flutter_blocs/controllers/user_api.controller.dart';
import 'package:flutter_blocs/models/returndata/returndata.model.dart';
import 'package:flutter_blocs/models/users/users.mode.dart';

class UsersResponsitory {
  static Future<List<UserModel>> getUsers()async{
    try {
      ReturnDataModel response = await UserApiController.getUsers();
      if(response.code == 1){
        return (response.data as List).map((e) => UserModel.fromJson(e)).toList();
      }
      return [];
    } catch (e) {
      throw e;
    }
  }
}